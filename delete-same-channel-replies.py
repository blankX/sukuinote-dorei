from pyrogram import Client, filters
from ... import log_errors, public_log_errors

@Client.on_message(~filters.sticker & filters.chat(-1001150433131) & ~filters.linked_channel)
@log_errors
@public_log_errors
async def delete_same_channel_replies(client, message):
    text = message.text or message.caption
    if not text:
        return
    reply = message.reply_to_message
    if getattr(reply, 'empty', True):
        return
    if not await filters.linked_channel_filter(None, None, reply):
        return
    reply_text = reply.text or reply.caption
    if text == reply_text:
        await message.delete()
