import html
from pyrogram import Client, filters
from ... import session, log_errors, public_log_errors

PRETTY_TAG_NAMES = {
    'tag': 'Tags',
    'artist': 'Artists',
    'parody': 'Parodies',
    'character': 'Characters',
    'group': 'Groups',
    'language': 'Languages',
    'category': 'Categories'
}

@Client.on_message(~filters.sticker & filters.chat(-1001150433131) & filters.linked_channel)
@log_errors
@public_log_errors
async def nhentai_comment_info(client, message):
    sent = set()
    text = message.text or message.caption or ''
    for i in text.split('\n'):
        for sauce in i.split(' '):
            if sauce.isnumeric() and sauce not in sent:
                sent.add(sauce)
                async with session.get(f'https://nhentai.net/api/gallery/{sauce}') as resp:
                    info = await resp.json()
                text = f'<b>{sauce}</b>\n<b>'
                if 'error' in info:
                    text += f'Error:</b> {html.escape(info.get("error") or "None")}'
                else:
                    text += 'Title:</b> '
                    if info['title']['english']:
                        text += html.escape(info['title']['english'])
                        if info['title']['japanese']:
                            text += '\n<b>Japanese Title:</b> '
                    if info['title']['japanese']:
                        text += html.escape(info['title']['japanese'])
                    tags = dict()
                    for i in info['tags']:
                        if i['type'] not in tags:
                            tags[i['type']] = []
                        tags[i['type']].append(i['name'])
                    for i in sorted(tags):
                        text += f'\n<b>{PRETTY_TAG_NAMES.get(i, i)}:</b> {html.escape(", ".join(tags[i]))}'
                    text += f'\n<b>Pages:</b> {info["num_pages"]}'
                await message.reply_text(text, disable_web_page_preview=True)
